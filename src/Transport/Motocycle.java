package Transport;

public class Motocycle implements ITransport {

	public Motocycle(String brand_name) {
		brand = brand_name;
		head = null;
		size = 0;
		lastModified = System.currentTimeMillis();
	}

	private int size = 0;
	private Model head;
	private long lastModified;

	private String brand;

	private class Model {
		public Model(String model_name, double price) {
			this.model_name = model_name;
			this.price = price;
		}

		String model_name = null;
		double price = Double.NaN;
		Model prev = null;
		Model next = null;
	}

	private void addNode(Model node) {
		if (node == null)
			throw new IllegalArgumentException();
		if (head != null) {
			head.prev = node;
			node.next = head;
			head = node;
		} else {
			head = node;
		}
		lastModified = System.currentTimeMillis();
		size++;
	}

	private void removeNode(Model node) {
		if (node == null)
			throw new NullPointerException();
		if (node == head) {
			head = head.next;
			if (head != null)
				head.prev = null;
		} else {
			node.prev.next = node.next;
			if (node.next != null)
				node.next.prev = node.prev;
			node = null;
		}
		lastModified = System.currentTimeMillis();
		size--;
	}

	private Model getNodeByModelName(String name) {
		Model node = head;
		while (node != null) {
			if (node.model_name == name)
				break;
			else
				node = node.next;
		}
		return node;
	}

	private Model getModelByName(String name) throws NoSuchModelNameException {
		Model node = getNodeByModelName(name);
		if (node != null)
			return node;
		else
			throw new NoSuchModelNameException();
	}

	public void renameModel(String old_name, String new_name)  throws NoSuchModelNameException,DuplicateModelNameException{
		Boolean name_is_unique = getNodeByModelName(new_name) == null;
		if (name_is_unique)
			getModelByName(old_name).model_name = new_name;
		else
			throw new DuplicateModelNameException();
		lastModified = System.currentTimeMillis();
	}

	public double getModelPrice(String name) throws NoSuchModelNameException{
		return getModelByName(name).price;
	}

	public void setModelPrice(String name, double new_price) throws NoSuchModelNameException{
		getModelByName(name).price = new_price;
		lastModified = System.currentTimeMillis();
	}

	public String[] getModelNames() {
		String[] result = new String[size];
		Model pointer = head;
		int i = 0;
		while (pointer != null) {
			result[i] = pointer.model_name;
			pointer = pointer.next;
			i++;
		}
		return result;
	}

	public double[] getModelPrices() {
		double[] result = new double[size];
		Model pointer = head;
		int i = 0;
		while (pointer != null) {
			result[i] = pointer.price;
			pointer = pointer.next;
			i++;
		}
		return result;
	}

	public void addNewModel(String name, double price) throws DuplicateModelNameException{
		Boolean name_is_unique = getNodeByModelName(name) == null;
		if (name_is_unique)
			addNode(new Model(name, price));
		else
			throw new DuplicateModelNameException();
	}

	public void deleteModel(String name) throws NoSuchModelNameException{
		removeNode(getModelByName(name));
	}

	public int countModels() {
		return size;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String new_brand) {
		if (new_brand.length() > 0) {
			brand = new_brand;
			lastModified = System.currentTimeMillis();
		} else
			throw new IllegalArgumentException("Название марки не может быть пустым");
	}

}
